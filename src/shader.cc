# include <iostream>

# include "shader.hh"
# include "utils.hh"

bool Shader::create() {
    // get sources of shaders in files
    std::string src = load(this->src_filename);
    // convert to char*
    const char *src_shader = src.c_str();

    // create empty shader object, returns the id != 0
    this->shader_id = glCreateShader(this->shader_type);

    // returns 0 if errors
    if (this->shader_id == 0) {
        std::cerr << "Error in creating shader " << this->src_filename << std::endl;
        return false;
    }

    // replace shader code by the content of strings
    glShaderSource(this->shader_id, 1, (const GLchar **) &(src_shader), nullptr);

    // compile le shader
    return this->compile();
}

bool Shader::compile() {
    GLint compile_status = GL_TRUE;

    // compile le shader
    glCompileShader(this->shader_id);
    // recup le status de la compilation
    glGetShaderiv(this->shader_id, GL_COMPILE_STATUS, &compile_status);

    // error ?
    if (compile_status != GL_TRUE) {
        GLint log_size;
        // recup la taille du log
        glGetShaderiv(this->shader_id, GL_INFO_LOG_LENGTH, &log_size);

        char *shader_log = (char *) std::malloc(log_size + 1); // +1 pour '\0'
        // erreur de malloc
        if (shader_log != nullptr) {
            // recup le log
            glGetShaderInfoLog(this->shader_id, log_size, &log_size, shader_log);
            // affiche le log sur stderr
            std::cerr << "Shader compile error:" << this->src_filename << ": "
                      << shader_log << std::endl;

            // free log
            std::free(shader_log);
        }

        return false;
    }

    return true;
}

void Shader::delete_shader() {
    if (this->shader_id != 0) {
        glDeleteShader(this->shader_id);
        this->shader_id = 0;
    }
}

bool Shader::link(GLint program_id) {
    GLint link_result = GL_TRUE;

    // link the shader on the program (pour que les shaders puissent etre run)
    glAttachShader(program_id, this->shader_id);

    // create executable of attached shaders
    glLinkProgram(program_id);
    // recup le result
    glGetProgramiv(program_id, GL_LINK_STATUS, &link_result);
    // error ?
    if (link_result != GL_TRUE) {
        GLint log_size;
        // get log size
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_size);

        char *link_log = (char *) std::malloc(log_size + 1); // +1 pour '\0'
        // malloc error ?
        if (link_log != nullptr) {
            // get log
            glGetProgramInfoLog(program_id, log_size, &log_size, link_log);
            // print log
            std::cerr << "Shader link error:" << this->src_filename << ": "
                      << link_log << std::endl;
            // free log
            std::free(link_log);
        }

        return false;
    }

    return true;
}
