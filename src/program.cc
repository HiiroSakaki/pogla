# include <iostream>

# include "program.hh"

Program::Program(std::vector<const char*> file_names, std::vector<int> shader_types)
{
    // create shaders
    for (auto i = 0u; i < file_names.size(); i++){
        Shader s(file_names[i], shader_types[i]);
        if (s.create())
            shaders.push_back(s);
        else
            goto error;
    }

    // create the program object
    this->program_id = glCreateProgram();

    // error checking
    if (this->program_id == 0)
    {
        std::cerr << "Can't init program" << std::endl;
        goto error;
    }

    // link
    for (auto &s : shaders) {
        if (!s.link(this->program_id))
            goto error;
    }
    return;

    // delete all
    glDeleteProgram(this->program_id);

error:
    std::cerr << "Error in creating program" << std::endl;
    for (auto &s : shaders) {
        s.delete_shader();
    }
    this->program_id = -1;
}
