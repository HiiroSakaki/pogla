# pragma once

# include <GL/glew.h>
# include <string>

class Shader
{
public:
    Shader(const std::string &file, GLenum type)
        : src_filename(file)
        , shader_id(0)
        , shader_type(type)
    {}

    bool create();
    bool compile();
    void delete_shader();
    bool link(GLint program_id);

    const std::string src_filename;
    GLuint shader_id;
    GLenum shader_type;
};
