# include <GL/glew.h>
# include <fstream>
# include <iostream>

// load shader from file
std::string load(const std::string &filename)
{
    std::ifstream input_src_file(filename, std::ios::in);
    std::string line;
    std::string file_content;

    if (input_src_file.fail()) {
        std::cerr << "FAIL\n";
        return "";
    }

    while (getline(input_src_file, line)) {
        file_content += line + "\n";
    }

    file_content += '\0';
    input_src_file.close();

    return file_content;
}

void test_opengl_error(const char *file, int line)
{
    // print error msg corresponding the error code
    GLenum err = glGetError();
    switch (err) {
        case GL_NO_ERROR:
            return;
        case GL_INVALID_ENUM:
            std::cerr << "GL_INVALID_ENUM: " << file << ", line: " << line << std::endl;
            break;
        case GL_INVALID_VALUE:
            std::cerr << "GL_INVALID_VALUE: " << file << ", line: " << line << std::endl;
            break;
        case GL_INVALID_OPERATION:
            std::cerr << "GL_INVALID_OPERATION: " << file << ", line: " << line << std::endl;
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION: " << file
                      << ", line: " << line << std::endl;
            break;
        case GL_OUT_OF_MEMORY:
            std::cerr << "GL_OUT_OF_MEMORY: " << file << ", line: " << line << std::endl;
            break;
        case GL_STACK_UNDERFLOW:
            std::cerr << "GL_STACK_UNDERFLOW: " << file << ", line: " << line << std::endl;
            break;
        case GL_STACK_OVERFLOW:
            std::cerr << "GL_STACK_OVERFLOW: " << file << ", line: " << line << std::endl;
            break;
        default:
            std::cerr << "UNKONWN ERROR: " << file << ", line: " << line << std::endl;
            break;
    }
    exit(2);
}
