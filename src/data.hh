# pragma once

# include <GL/glew.h>

# include <vector>
# include <string>

class Data
{
public:
    Data(GLint program_id, const std::vector<std::string> &fields,
         const std::vector<std::vector<GLfloat>> &datas);

    void display() const;

    GLuint vba_id;
    std::vector<std::vector<GLfloat>> datas;

private:
    std::vector<std::string> fields;
    std::vector<GLint> locations;
    std::vector<GLuint> vbo_ids;

    GLint program_id;
};
