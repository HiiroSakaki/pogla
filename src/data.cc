# include "data.hh"
# include "utils.hh"

Data::Data(GLint program_id, const std::vector<std::string> &fields,
           const std::vector<std::vector<GLfloat>> &datas)
{
    this->program_id = program_id;
    this->fields = fields;
    this->datas = datas;

    // get the locations in program
    for (auto &field: fields)
        this->locations.push_back(glGetAttribLocation(this->program_id, field.c_str()));

    // gen the vba id
    glGenVertexArrays(1, &this->vba_id);

    // bind to object
    glBindVertexArray(this->vba_id);

    // create buffers
    this->vbo_ids.reserve(fields.size());
    glGenBuffers(fields.size(), this->vbo_ids.data());
    test_opengl_error(__FILE__, __LINE__);

    for (size_t i = 0; i < fields.size(); ++i)
    {
        // set buffer to array type
        glBindBuffer(GL_ARRAY_BUFFER, this->vbo_ids[i]);

        // set buffer data as vertexlist
        glBufferData(GL_ARRAY_BUFFER,
                     datas[i].size() * sizeof(GLfloat),
                     datas[i].data(),
                     GL_STATIC_DRAW);

        // a point = 3 float
        glVertexAttribPointer(this->locations[i], 3, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(this->locations[i]);
        test_opengl_error(__FILE__, __LINE__);
    }

    glLineWidth(1.4f);

    // end of object
    glBindVertexArray(0);
}

void Data::display() const
{
    glUseProgram(this->program_id);
    glBindVertexArray(this->vba_id);
    glDrawArrays(GL_POINTS, 0, this->datas[0].size()); // positions
    glBindVertexArray(0);
}
