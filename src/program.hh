# pragma once

# include <GL/glew.h>
#include <vector>
#include <unordered_map>
# include "shader.hh"


class Program
{
public:
    Program(std::vector<const char*> file_names, std::vector<int> shader_types);

    GLint program_id;
private:
    std::vector<Shader> shaders;
};
