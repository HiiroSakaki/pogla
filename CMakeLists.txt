cmake_minimum_required(VERSION 3.0)
set(CMAKE_CXX_COMPILER g++)
project(POGL LANGUAGES CXX)

set(CMAKE_CXX_FLAGS_DEBUG  "-Wall -Wextra -g -std=c++17")
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -Wextra -std=c++17 -O3 -m64 -march=native -fopt-info-vec-optimized -fopt-info-vec-missed -ftree-vectorize")
set(CMAKE_EXE_LINKER_FLAGS "-lGL  -lGLEW -lglut -lpthread -lSOIL")

add_subdirectory(src)
include_directories(src)

add_executable(main main.cc)
target_link_libraries(main lib_src)
