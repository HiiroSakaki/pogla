# include <GL/glew.h>
# include <GL/freeglut.h>
# include <iostream>
# include <memory>
# include <vector>
# include <glm/glm.hpp>
# include <glm/gtc/matrix_transform.hpp>
# include <random>
# include <algorithm>

# include "data.hh"
# include "program.hh"
# include "utils.hh"

#define WIDTH 512
#define HEIGHT 512

std::shared_ptr<Data> data;
std::shared_ptr<Program> program;

glm::vec3 cameraPos = glm::vec3(7.0f, 1.0f, 5.0f);
glm::vec3 cameraFront = glm::vec3(-1.0f, -0.1f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

glm::vec2 mouse_pos = glm::vec2(WIDTH / 2, HEIGHT / 2);

bool firstMouse = true;
float yaw = -90.0f;
float pitch = 0.0f;
float lastX = 800.0f / 2.0;
float lastY = 600.0 / 2.0;
float fov = 45.0f;
float anim_time;

bool full_screen_mode = false;

void init_matrices();

void anim() {
    GLint anim_time_location;
    anim_time_location =
            glGetUniformLocation(program->program_id, "anim_time");
    glUniform1f(anim_time_location, anim_time);
    anim_time += 0.1;
    if (anim_time > 6.3)
        anim_time = 0;
    glutPostRedisplay();
}

void timer(int value) {
    anim();
    glutTimerFunc(50, timer, 0);
}

void init_anim() {
    glutTimerFunc(50, timer, 0);
}

void window_resize(int width, int height) {
    glViewport(0, 0, width, height);
    test_opengl_error(__FILE__, __LINE__);
}

void GLAPIENTRY
MessageCallback([[maybe_unused]] GLenum source,
                GLenum type,
                [[maybe_unused]] GLuint id,
                GLenum severity,
                [[maybe_unused]] GLsizei length,
                const GLchar *message,
                [[maybe_unused]] const void *userParam) {
    fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type, severity, message);
}


void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    glUniformMatrix4fv(glGetUniformLocation(program->program_id, "view_matrix"), 1, GL_FALSE, &view[0][0]);
    data->display();
    glFinish();
    glutSwapBuffers();
    test_opengl_error(__FILE__, __LINE__);
}

void keyboard_letters(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'f' :
        case 'F' :
            if (!full_screen_mode)
                glutFullScreen();
            else
                glutReshapeWindow(WIDTH, HEIGHT);
            full_screen_mode = !full_screen_mode;
            break;

        case 'q' :
        case 'Q' :
        case 27  : // ESC
            exit (0);
            break;

        default :
            break;
    }
}

void keyboard_special_keys(int key, int x, int y)
{
    glm::vec3 left = glm::normalize(glm::cross(cameraUp, cameraFront));
    switch (key)
    {
        case GLUT_KEY_UP :
            cameraPos += glm::vec3(0, 1, 0);
            break;

        case GLUT_KEY_DOWN :
            cameraPos += glm::vec3(0, -1, 0);
            break;

        case GLUT_KEY_LEFT :
            cameraPos += glm::vec3(0.8 * left.x, 0.8 * left.y, 0.8 * left.z);
            break;

        case GLUT_KEY_RIGHT :
            cameraPos -= glm::vec3(0.8 * left.x, 0.8 * left.y, 0.8 * left.z);
            break;

        default :
            break;
    }
}

void mouse_callback(int button, int state, int xpos, int ypos) {
    // mouse wheel events
    if (button == 3)
        cameraPos += glm::vec3(0.5 * cameraFront.x, 0.5 * cameraFront.y, 0.5 * cameraFront.z);
    else if (button == 4)
        cameraPos -= glm::vec3(0.5 * cameraFront.x, 0.5 * cameraFront.y, 0.5 * cameraFront.z);
}

void mouse_drag(int x, int y)
{
    glm::vec2 old_mouse_pos = mouse_pos;

    mouse_pos = glm::vec2(x, y);

    if (mouse_pos.x - old_mouse_pos.x > 0)
    {
        // move camera to left
        cameraFront += glm::vec3(0.01, 0, 0);
    }
    else if (mouse_pos.x - old_mouse_pos.x < 0)
    {
        // move camera to right
        cameraFront -= glm::vec3(0.01, 0, 0);
    }

    if (mouse_pos.y - old_mouse_pos.y > 0)
    {
        // move camera to up
        cameraFront -= glm::vec3(0, 0.01, 0);
    }
    else if (mouse_pos.y - old_mouse_pos.y < 0)
    {
        // move camera to down
        cameraFront += glm::vec3(0, 0.01, 0);
    }
}


void initGlut(int &argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitContextVersion(4, 5); // OpenGL version 4.5
    glutInitContextProfile(GLUT_CORE_PROFILE | GLUT_DEBUG);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT); // window size
    glutInitWindowPosition(100, 100); // window position
    glutCreateWindow("Our Fire"); // title of window
    // display
    glutDisplayFunc(display);
    glutReshapeFunc(window_resize);

    // keyboard
    glutKeyboardFunc(keyboard_letters);
    glutSpecialFunc(keyboard_special_keys);

    // mouse
    glutMouseFunc(mouse_callback);
    glutMotionFunc(mouse_drag);

    init_anim();
}

bool initGlew() {
    if (glewInit()) {
        std::cerr << " Error while initializing glew";
        return false;
    }
    return true;
}

void initGL() {
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_CULL_FACE);

    // set color to grey
    glClearColor(0.3, 0.3, 0.3, 1.0);

    // set alignment requirements for the start of each pixel row in memory
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);

    test_opengl_error(__FILE__, __LINE__);
}

void init_data() {

    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> n{0, 0.06};
    std::normal_distribution<> c{0.05, 0.1};

    // background
    std::vector<std::string> fields = {
            "position",
            "color_modifier",
            "anim_modifier"
    };
    std::vector<std::vector<GLfloat>> datas = {{},
                                               {},
                                               {}};
    for (auto x = -5.f; x <= 5.f; x += 0.075f) {
        for (auto z = -5.f; z <= 5.f; z += 0.075f) {
            datas[0].push_back(x + n(gen));
            datas[0].push_back(abs(3 * n(gen)));
            datas[0].push_back(z + n(gen));
        }
    }
    for (auto i = 0u; i < datas[0].size(); i++) {
        datas[1].push_back(c(gen));
        datas[2].push_back(c(gen));
    }
    std::sort(datas[2].begin(), datas[2].end());
    data = std::make_shared<Data>(program->program_id, fields, datas);
}

void init_matrices() {
    glUseProgram(program->program_id);
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float) 1920 / (float) 1080, 0.1f, 1000.0f);
    glUniformMatrix4fv(glGetUniformLocation(program->program_id, "projection_matrix"), 1, GL_FALSE, &projection[0][0]);
    glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    glUniformMatrix4fv(glGetUniformLocation(program->program_id, "view_matrix"), 1, GL_FALSE, &view[0][0]);
}

int main(int argc, char *argv[]) {
    // Glut init
    initGlut(argc, argv);
    std::cerr << "Glut init Ok\n";

    // Glew init
    initGlew();
    std::cerr << "Glew init Ok\n";

    // GL init
    initGL();
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, nullptr);
    std::cerr << "GL init Ok\n";

    // init program shaders
    program = std::make_shared<Program>(
            std::vector<const char *>{"../shaders/vs.shd", "../shaders/gs.shd", "../shaders/fs.shd"},
            std::vector<int>{GL_VERTEX_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER});
    std::cerr << "init shaders Ok\n";

    // init data
    init_data();

    // init camera
    init_matrices();

    std::cerr << "init data Ok\n";
    std::cerr << "Start\n";

    // glut event loop
    glutMainLoop();
    return 0;
}
